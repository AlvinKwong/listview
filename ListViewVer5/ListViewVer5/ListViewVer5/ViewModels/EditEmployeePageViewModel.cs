﻿using ListViewVer5.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Windows.Input;
using System.IO;

namespace ListViewVer5.ViewModels
{
    public class EditEmployeePageViewModel : BindableBase, INavigationAware
    {
        #region parameters
        private Employee employee;
        public Employee Employee
        {
            get { return employee; }
            set { SetProperty(ref employee, value); }
        }
        #endregion

        DelegateCommand _cancelCommand;
        //DelegateCommand _calculateAgeCommand;
        private INavigationService _navigationService;
        Employee BackUp;
        public EditEmployeePageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;

        }
        public ICommand CancelCommand
        {
            get
            {
                return _cancelCommand ??
                    (_cancelCommand = new DelegateCommand(RestoreInfo));
            }
        }
        void RestoreInfo()
        {
            //Employee = BackUp;
            Employee.Name = BackUp.Name;
            Employee.Age = BackUp.Age;
            Employee.Sex = BackUp.Sex;
            Employee.Birthday = BackUp.Birthday;
            Employee.Salary = BackUp.Salary;
            _navigationService.GoBackAsync();
        }
        private void CalculateAge()
        {
            var today = DateTime.Today;
            int age = today.Year - Employee.Birthday.Year;
            if (Employee.Birthday > today.AddYears(-age))
                age--;
            Employee.Age = age;
        }
        public EditEmployeePageViewModel()
        {
            
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }
        public void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("employee"))
            {
                BackUp = new Employee((Employee)parameters["employee"]);
                Employee = (Employee)parameters["employee"];
            }
        }
        
    }
}

