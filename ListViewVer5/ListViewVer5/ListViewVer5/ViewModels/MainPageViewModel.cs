﻿using ListViewVer5.Model;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace ListViewVer5.ViewModels
{
    public class MainPageViewModel : BindableBase, INavigationAware
    {
        private readonly IJsonFile _jFile;
        const string fileName = "EmployeeList";

        private string employeeName = "";
        private int employeeAge;
        private Boolean employeeGender = true;
        private ObservableCollection<Employee> _employees;

        DelegateCommand _applyEmployeeCommand;
        //DelegateCommand _saveListCommand;
        DelegateCommand _loadListCommand;
        DelegateCommand<Employee> _editEmployeeCommand;
        public ObservableCollection<Employee> Employees
        {
            get { return _employees; }
            set { _employees = value; Xamarin.Forms.Application.Current.Properties["Employees"] = Employees; }
        }

        public ICommand ApplyEmployeeCommand
        {
            get
            {
                return _applyEmployeeCommand ??
                    (_applyEmployeeCommand = new DelegateCommand(AddEmployee, CanExecute).ObservesProperty(() => EmployeeName).ObservesProperty(() => EmployeeAge).ObservesProperty(() => EmployeeGender));
            }
        }
        private Boolean CanExecute()
        {
            if ((EmployeeAge >= 20) && (employeeAge <= 65) && EmployeeName != "")
                return true;
            return false;
        }
        public class ItemTappedEventArgs : EventArgs
        {
            public object Item { get; }
            public object Group { get; }
        }
        public ICommand EditEmployeeCommand
        {
            get
            {
                return _editEmployeeCommand ??
                    (_editEmployeeCommand = new DelegateCommand<Employee>(EditEmployee));
            }
        }

        //public ICommand SaveListCommand
        //{
        //    get
        //    {
        //        return _saveListCommand ?? (_saveListCommand = new DelegateCommand(SaveList));
        //    }
        //}
        public ICommand LoadListCommand
        {
            get
            {
                return _loadListCommand ?? (_loadListCommand = new DelegateCommand(LoadList));
            }
        }

        void EditEmployee(Employee employee)
        {
            var NavPar = new NavigationParameters { { "employee", employee } };
            _navigationService.NavigateAsync("EditEmployeePage", NavPar);
        }
        void LoadList()
        {
            Employees = Employee.Load();
        }

        //void SaveList()
        //{
        //    Employee.SaveList();
        //}

        public string EmployeeName
        {
            get
            { return employeeName; }
            set
            {      
                SetProperty(ref employeeName, value);
            }
        }
        public int EmployeeAge
        {
            get
            { return employeeAge; }
            set
            {
                SetProperty(ref employeeAge, value);
            }
        }
        public Boolean EmployeeGender
        {
            get
            { return employeeGender; }
            set
            {
                SetProperty(ref employeeGender, value);
            }
        }     

        private INavigationService _navigationService;
        public MainPageViewModel(INavigationService navigationService, IJsonFile jFile)
        {
            _jFile = jFile;
            _navigationService = navigationService;
                        
            Employees = Employee.Load();
        }
        
        void AddEmployee()
        {
            Employees.Add(new Employee(EmployeeName, EmployeeAge, EmployeeGender));
            //SaveList();
        }

        public MainPageViewModel()
        {

        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
        }
        
    }
}
