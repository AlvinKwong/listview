﻿using Prism.Unity;
using ListViewVer5.Views;
using Xamarin.Forms;
using ListViewVer5.ViewModels;
using ListViewVer5.Model;

namespace ListViewVer5
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();
            NavigationService.NavigateAsync("NavigationPage/MainPage");
            
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<EditEmployeePage>();
        }

        protected override void OnSleep()
        {
            Employee.SaveList();
            base.OnSleep();
        }
    }
}
