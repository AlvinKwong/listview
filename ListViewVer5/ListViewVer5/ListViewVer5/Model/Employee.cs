﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListViewVer5.Model
{
    public class Employee : BindableBase, INotifyPropertyChanged
    {
        private const string FileName = "EmployeeList";

        private string _name = "Name";
        private int _age = 0;
        private Boolean _sex = true;
        private DateTime _birthday = DateTime.Today;
        private int _salary = 100000;
        private static ObservableCollection<Employee> _employees = new ObservableCollection<Employee>();
        
        public string Name { get { return _name; } set { SetProperty(ref _name, value); } }
        public int Age { get { return _age; } set { SetProperty(ref _age, value); } }
        public Boolean Sex { get { return _sex; } set { SetProperty(ref _sex, value); } }
        public DateTime Birthday { get { return _birthday; } set { SetProperty(ref _birthday, value); CalculateAge(); } }
        public int Salary { get { return _salary; } set { SetProperty(ref _salary, value); } }
        public static ObservableCollection<Employee> Employees {
            get
            {
                if (_employees == null)
                    _employees = new ObservableCollection<Employee>();
                return _employees;
            }
        }

        #region Constructor
        public Employee()
        {
            this.Name = "Unnamed";
            this.Age = 0;
            this.Sex = true;
            this.Birthday = DateTime.Today;
            this.Salary = 100000;
        }

        public Employee(Employee employee)
        {
            Name = employee.Name;
            Age = employee.Age;
            Sex = employee.Sex;
            Birthday = employee.Birthday;
            Salary = employee.Salary;
        }
        
        public Employee(string name, int age)
        {
            this.Name = name;
            this.Age = age;
            this.Sex = true;
            this.Birthday = DateTime.Today;
            this.Salary = 100000;
        }

        public Employee(string name, int age, Boolean sex)
        {
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
            this.Birthday = DateTime.Today;
            this.Salary = 100000;
        }

        public Employee(string name, int age, Boolean sex, DateTime birthday, int salary)
        {
            this.Name = name;
            this.Age = age;
            this.Sex = sex;
            this.Birthday = birthday;
            this.Salary = salary;
        }
        
        #endregion        

        private void CalculateAge()
        {
            var today = DateTime.Today;
            int age = today.Year - Birthday.Year;
            if (Birthday > today.AddYears(-age))
                age--;
            Age = age;
        }

        public static void SaveList()
        {
            var json = DependencyService.Get<IJsonFile>();

            var str = JsonConvert.SerializeObject(_employees);

            json.SaveJsonFile(FileName, str);
        }

        private static void LoadList()
        {
            var json = DependencyService.Get<IJsonFile>();

            var str = json.LoadJsonFile(FileName);

            if (!(string.IsNullOrEmpty(str) || str == "null"))            
            {
                var result = JsonConvert.DeserializeObject<List<Employee>>(str);
                _employees.Clear();
                foreach (Employee e in result)
                    _employees.Add(e);
            }
        }

        public static ObservableCollection<Employee> Load()
        {
            LoadList();            
            return _employees;
        }
    }
}