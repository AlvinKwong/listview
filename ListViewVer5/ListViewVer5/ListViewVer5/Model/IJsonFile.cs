﻿namespace ListViewVer5.Model
{
    public interface IJsonFile
    {
        bool SaveJsonFile(string fileName, string json);
        string LoadJsonFile(string fileName);
        bool ClearJsonFile(string fileName);
    }
}
