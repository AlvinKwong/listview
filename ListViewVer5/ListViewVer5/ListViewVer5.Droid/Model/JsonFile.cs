﻿using System;
using Java.Lang;
using ListViewVer5.Model;
using System.IO;
using ListViewVer5.Droid.Model;

[assembly: Xamarin.Forms.Dependency(typeof(JsonFile))]
namespace ListViewVer5.Droid.Model
{
    public class JsonFile : IJsonFile
    {
        private static readonly string Tag = typeof(JsonFile).Name;

        private static string MakeFilePath(string fileName)
        {
            var folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(folder, fileName);
        }
        public JsonFile()
        {
        }
        public bool SaveJsonFile(string fileName, string json)
        {
            var path = MakeFilePath(fileName);

            try
            {
                File.WriteAllText(path, json);
            }
            catch (System.Exception e)
            {
                Android.Util.Log.Debug(Tag, e.Message);
                return false;
            }
            return true;
        }        

        public string LoadJsonFile(string fileName)
        {
            var path = MakeFilePath(fileName);

            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }


            return null;
        }

        public bool ClearJsonFile(string fileName)
        {
            var path = MakeFilePath(fileName);

            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (System.Exception e)
                {
                    Android.Util.Log.Debug(Tag, e.Message);
                    return false;
                }

                return true;
            }

            return false;
        }
    }
}