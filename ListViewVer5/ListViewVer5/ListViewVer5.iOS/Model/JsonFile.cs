﻿using ListViewVer5.iOS.Model;
using ListViewVer5.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(JsonFile))]
namespace ListViewVer5.iOS.Model
{
    public class JsonFile : IJsonFile
    {
        private static string MakeFilePath(string fileName)
        {
            var folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            return Path.Combine(folder, fileName);
        }
        public JsonFile()
        {
        }
        public bool SaveJsonFile(string fileName, string json)
        {
            var path = MakeFilePath(fileName);

            try
            {
                File.WriteAllText(path, json);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                return false;
            }
            return true;
        }

        public string LoadJsonFile(string fileName)
        {
            var path = MakeFilePath(fileName);

            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }


            return null;
        }

        public bool ClearJsonFile(string fileName)
        {
            var path = MakeFilePath(fileName);

            if (File.Exists(path))
            {
                try
                {
                    File.Delete(path);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    return false;
                }

                return true;
            }

            return false;
        }
    }
}
